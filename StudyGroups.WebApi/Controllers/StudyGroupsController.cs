﻿using Microsoft.AspNetCore.Mvc;
using StudyGroups.WebApi.Business;
using StudyGroups.WebApi.Database.Models;
using System.Collections.Generic;

namespace StudyGroups.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudyGroupsController : ControllerBase
    {
        private readonly IStudyGroupService _studyGroupService;

        public StudyGroupsController(IStudyGroupService studyGroupService)
        {
            _studyGroupService = studyGroupService;
        }

        [HttpPost]
        public IActionResult CreateGroup(List<Card> cards)
        {
            _studyGroupService.Add(cards);

            return StatusCode(201);
        }

        [HttpGet("{groupName}")]
        public IActionResult GetGroup(string groupName)
        {
            return Ok(_studyGroupService.GetGroup(groupName));
        }

        [HttpGet("group-names")]
        public IActionResult GetGroupNames()
        {
            return Ok(_studyGroupService.GetGroupNames());
        }

        [HttpDelete("{groupName}")]
        public IActionResult DeleteGroup(string groupName)
        {
            _studyGroupService.DeleteGroup(groupName);
            return Ok(groupName);
        }
    }
}
