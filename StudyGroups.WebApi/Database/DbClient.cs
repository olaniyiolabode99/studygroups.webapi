﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using StudyGroups.WebApi.Database.Models;

namespace StudyGroups.WebApi.Database
{
    public class DbClient : IDbClient
    {
        private readonly IMongoDatabase _database;

        public DbClient(IOptions<DbConfig> dbConfig)
        {
            var client = new MongoClient(dbConfig.Value.Connection_String);
            _database = client.GetDatabase(dbConfig.Value.Database_Name);
        }

        public IMongoCollection<Card> GetCardsCollection() =>
            _database.GetCollection<Card>("Cards");
    }
}
